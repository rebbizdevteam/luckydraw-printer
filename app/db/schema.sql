CREATE TABLE "receipts" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "person_name" TEXT(255,0) NOT NULL,
    "contact_number" TEXT(255,0) NOT NULL,
    "exhibitor" TEXT(255,0) NOT NULL,
    "total_amount" INTEGER NOT NULL,
    "qty" INTEGER NOT NULL
);

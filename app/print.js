'use strict'
const electron = require('electron')
const BrowserWindow = electron.BrowserWindow

module.exports.openDialog = function (event,data) {
  let print_win;
  print_win = new BrowserWindow({'auto-hide-menu-bar':true, x:100, y:100, width: 300, height: 250});
  print_win.loadURL(`file://${__dirname}/html/print.html`,{"extraHeaders" : "pragma: no-cache\n"});
  let doc = print_win.webContents
  print_win.webContents.on('did-finish-load', function() {
    print_win.webContents.send('print-data', data)
  });
  print_win.on('close', function(){print_win = null});
}

'use strict'

const fs = require('fs')
const path = require('path')
const app = require('electron').remote.app
const cheerio = require('cheerio')
const ipc = require('electron').ipcRenderer

window.$ = window.jQuery = require('jquery')
window.Tether = require('tether')
window.Bootstrap = require('bootstrap')

let webRoot = path.dirname(__dirname)
window.view = require(path.join(webRoot, 'view.js'))
window.model = require(path.join(webRoot, 'model.js'))
window.model.db = path.join(app.getPath('userData'), 'luckydraw.db')

// Compose the DOM from separate HTML concerns; each from its own file.
let htmlPath = path.join(app.getAppPath(), 'app', 'html')
let body = fs.readFileSync(path.join(htmlPath, 'body.html'), 'utf8')
let navBar = fs.readFileSync(path.join(htmlPath, 'nav-bar.html'), 'utf8')
let menu = fs.readFileSync(path.join(htmlPath, 'menu.html'), 'utf8')
let receipt = fs.readFileSync(path.join(htmlPath, 'receipt.html'), 'utf8')
let editReceipt = fs.readFileSync(path.join(htmlPath, 'edit-receipt.html'), 'utf8')

let O = cheerio.load(body)
O('#nav-bar').append(navBar)
O('#menu').append(menu)
O('#receipt').append(receipt)
O('#edit-receipt').append(editReceipt)

// Pass the DOM from Cheerio to jQuery.
let dom = O.html()
$('body').html(dom)

$('document').ready(function () {
  window.model.getAllReceipts()
  window.view.refreshSumAmount()
  $("#total_amount").focusout(function(){
    var amount = this.value
    if(amount == 0){
      $("#qty").val(0)
    }else if(amount <= 500000){
      $("#qty").val(1)
    }else if(amount > 500000 && amount <= 3000000){
      $("#qty").val(3)
    }else if(amount > 3000000 && amount < 10000000){
      $("#qty").val(5)
    }else if(amount >= 10000000){
      $("#qty").val(10)
    }else if(isNaN(amount)==false){
      $("#qty").val(0)
    }         
    console.log(amount);
    this.value = parseInt(this.value).toLocaleString()
  })
  $("#total_amount").focus(function(){
    $("#total_amount").val(this.value.replace(/,/g, "").replace(" ",""));    
    $("#qty").val(0)
  }) 
  $('#edit-receipt-submit').click(function (e) {
    e.preventDefault()
    let ok = true
    $('#name, #contact_number', "#exhibitor","#qty", "#total_amount").each(function (idx, obj) {
      if ($(obj).val() === '') {
        $(obj).removeClass('is-valid').addClass('is-invalid')
        ok = false
      } else {
        $(obj).addClass('is-valid').removeClass('is-invalid')
      }
    })
    if (ok) {
      $('#edit-receipt-form').addClass('was-validated')
      $('.shown-for-edit').show()
      let formId = $(e.target).parents('form').attr('id')
      $("#total_amount").val($("#total_amount").val().replace(/\D/g,''));
      let keyValue = window.view.getFormFieldValues(formId)
      window.model.saveFormData('receipts', keyValue, function () {
        window.model.getAllReceipts()
        window.view.refreshSumAmount()
      })
    }
  })
  $('#edit-and-print-receipt-submit').click(function (e) {
    e.preventDefault()
    let ok = true
    $('#name, #contact_number', "#exhibitor","#qty", "#total_amount").each(function (idx, obj) {
      if ($(obj).val() === '') {
        $(obj).removeClass('is-valid').addClass('is-invalid')
        ok = false
      } else {
        $(obj).addClass('is-valid').removeClass('is-invalid')
      }
    })
    if (ok) {
      $('#edit-receipt-form').addClass('was-validated')
      $('.shown-for-edit').show()
      let formId = $(e.target).parents('form').attr('id')
      $("#total_amount").val($("#total_amount").val().replace(/\D/g,''));
      let keyValue = window.view.getFormFieldValues(formId)
      window.model.saveFormData('receipts', keyValue, function () {
        window.model.getAllReceipts()
        window.view.refreshSumAmount()
      });
      let formDataHash = {}
      let formData = $("form#edit-receipt-form").serializeArray();
      $.each(formData,function(i, v) {formDataHash[v.name] = v.value;});
      if(formDataHash.id == ""){
        let data = model.getLastReceipt();
        let dataHash = data[0]
        ipc.send('print-doc',dataHash)
      }else{
        ipc.send('print-doc',formDataHash)
      }
    }
  })
})

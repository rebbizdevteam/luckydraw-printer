'use strict'
const electron = require('electron')
const path = require('path')
const model = require(path.join(__dirname, 'model.js'))

module.exports.showReceipts = function (rowsObject) {
  let markup = ''
  for (let rowId in rowsObject) {
    let row = rowsObject[rowId]
    markup += '<tr id=pid_'+row.id +'><td>' + row.id + '</td>'+
    '<td>' + row.person_name + '</td>' +
    '<td>' + row.contact_number + '</td>' +
    '<td>' + row.exhibitor + '</td>' +  
    '<td>' + row.total_amount.toLocaleString() + '</td>' +  
    '<td>' + row.qty + '</td>' +
    '<td><a href="#"><img id="edit-pid_' +
    row.id + '" class="icon edit" src="' +
    path.join(__dirname, 'img', 'edit-solid.svg') + '"></a>' +
    '<a href="#"><img id="del-pid_' + row.id +
    '" class="icon delete" src="' + path.join(__dirname, 'img', 'times-solid.svg') +
    '"></a>'+
    '<a href="#"><img id="print-pid_' +
    row.id + '" class="icon print" src="' +
    path.join(__dirname, 'img', 'print-solid.svg') + '"></a></td></tr>'
  }

  $('#add-receipt, #edit-receipt, #print-receipt').hide()
  $('#receipt-list').html(markup)
  $('a.nav-link').removeClass('active')
  $('a.nav-link.receipt').addClass('active')
  $('#receipt').show()
  $('#receipt-list img.edit').each(function (idx, obj) {
    $(obj).on('click', function () {
      window.view.editReceipt(this.id)
    })
  })
  $('#receipt-list img.delete').each(function (idx, obj) {
    $(obj).on('click', function () {
      window.view.deleteReceipt(this.id)
    })
  })
  $('#receipt-list img.print').each(function (idx, obj) {
    $(obj).on('click', function () {
      window.view.printReceipt(this.id)
    })
  })
}

module.exports.listReceipt = function (e) {
  $('a.nav-link').removeClass('active')
  $(e).addClass('active')
  $('#edit-receipt').hide()
  $('#print-receipt').hide()
  window.model.getReceipt()
  $('#receipt').show()
}

module.exports.addReceipt = function (e) {
  $('a.nav-link').removeClass('active')
  $(e).addClass('active')
  $('#receipt').hide()
  $('.shown-for-edit').show()
  $('#edit-receipt h2').html('Add Receipt')
  $('#edit-receipt-submit').html('Save')
  $('#edit-and-print-receipt-submit').html('Save and Print')
  $('#edit-receipt-form input').val('')
  $('#edit-receipt-form').removeClass('was-validated')
  $('#name, #contact_number')
    .removeClass('is-valid is-invalid')
  $('#receipt_id').parent().hide()
  $('#edit-receipt').show()
}

module.exports.editReceipt = function (pid) {
  $('#edit-receipt h2').html('Edit Receipt')
  $('#edit-receipt-submit').html('Update')
  $('.shown-for-edit').show()
  $('#edit-and-print-receipt-submit').html('Update and Print')
  $('#edit-receipt-form').removeClass('was-validated')
  $('#name, #contact_number')
    .removeClass('is-valid is-invalid')
  $('#id').parent().show()
  pid = pid.split('_')[1]
  let row = model.getReceipt(pid)[0]
  $('#id').val(row.id)
  $('#person_name').val(row.person_name)
  $('#contact_number').val(row.contact_number)
  $('#exhibitor').val(row.exhibitor)
  $('#total_amount').val(row.total_amount)
  $('#qty').val(row.qty)
  $('#receipt, #add-receipt').hide()
  $('#edit-receipt').show()
}

module.exports.deleteReceipt = function (pid) {
  model.deleteReceipt(pid.split('_')[1], document.getElementById("pid_"+pid.split('_')[1]).remove());
}

module.exports.printReceipt = function(pid) {
  $('#edit-and-print-receipt-submit').html(' Print')
  $('#edit-receipt-submit').hide()
  $('#edit-receipt-form').removeClass('was-validated')
  $('#name, #contact_number')
    .removeClass('is-valid is-invalid')
  $('#id').parent().show()
  pid = pid.split('_')[1]
  let row = model.getReceipt(pid)[0]
  $('#edit-receipt h2').html('Print Receipt '+row.person_name)
  $('.shown-for-edit').hide()
  $('#id').val(row.id)
  $('#person_name').val(row.person_name)
  $('#contact_number').val(row.contact_number)
  $('#exhibitor').val(row.exhibitor)
  $('#total_amount').val(row.total_amount)
  $('#qty').val(row.qty)
  $('#receipt, #add-receipt').hide()
  $('#edit-receipt').show()
}

module.exports.getFormFieldValues = function (formId) {
  let keyValue = {columns: [], values: []}
  $('#' + formId).find('input:visible, textarea:visible').each(function (idx, obj) {
    keyValue.columns.push($(obj).attr('id'))
    keyValue.values.push($(obj).val())
  })
  return keyValue
}

module.exports.refreshSumAmount = function () {
  var all_amount = window.model.sumTotalAmount()
  if(all_amount != "" ){

    $("#all_amount").text(Math.floor(parseInt(all_amount[0].total)/100000).toLocaleString() + " Lakh");
  }
}
